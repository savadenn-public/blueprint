# Horaires individualisés

Du fait que chacun est en horaires individualisés on priorise le travail asynchrone avec aller-retours dans les tickets.  
L’assigné d’une tâche prend les décisions avec les avis dans le ticket. Quand il y a besoin d’avis, il est important de préciser jusque quand ces derniers sont recueillis afin que tout le monde puisse s’organiser.

Chacun renseigne ses indisponibilités dans l’[agenda](https://calendar.google.com/).  

Dans le cas où un point synchrone est nécessaire : 

1. Planifier le point dans l’[agenda](https://calendar.google.com/)
1. **Prévenir** en cas absence imprévue : afin de replanifier la réunion (si cela est possible et nécessaire) et par respect pour les collègues
1. De manière générale : si quelqu’un est absent, on avance sans lui