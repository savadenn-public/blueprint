# `::sales` {#sales-title}
`ventes`{#sales alias-for=sales-title}

Les `::sales` sont gérés dans [Hubspot](https://app.hubspot.com).

Le [tableau des transactions](https://app.hubspot.com/contacts/14558262/deals/board/view/all/) donne l’état d'avancement des différents prospects.