# `::expenses | capital` {#expenses-title}
`dépense`{#expense alias-for=expenses}
`dépenses`{#expenses alias-for=expenses}

## Gestion des `::expenses`

_Chaque étape des dépenses est géré par un tag dans `::gitlab`_

1. [![Accounting planned](./tags/accounting-1_planned.svg)](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=accounting%3A%3A1+planned)
   * Tu as prévu une `::expense` pour cette `::issue`
1. [![Accounting quoted](./tags/accounting-2_quoted.svg)](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=accounting%3A%3A2+quoted)
   * Tu as ajouté le ou les devis à la `::issue`
1. [![Accounting spent](./tags/accounting-3_spent.svg)](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=accounting%3A%3A3+spent)
   * Tu as fait l’achat
1. [![Accounting invoiced](./tags/accounting-4_invoiced.svg)](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=accounting%3A%3A4+invoiced)
   * Tu as ajouté la ou les factures dans [le dossier Achat du mois](https://nextcloud.savadenn.bzh/f/5737) que tu as créé si besoin (format _YYYY-MM_)
   * Tu as ajouté le ou les liens vers les factures dans la `::issue`

::: note
Il est préférable de faire les achats avec des moyens de paiement de Savadenn. Mais si tu préfères les commerces de proximité, utilise la `::personnalExpense-process | lower`.
:::

## Gestion `::accountant` des  `::expenses`

::: note
Les étapes suivantes sont fait par le gérant pour le moment.
:::

1. [![Accounting registered](./tags/accounting-5_registered.svg)](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=accounting%3A%3A5+registered)
   * Tu as transmis les factures au `::accountant`
1. [![Accounting done](./tags/accounting-6_done.svg)](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=accounting%3A%3A6+done)
   * Tu as eu confirmation (ou vérifié) que la comptabilité est à jour
   
## Gestion des `::personnalExpense` {#personnalExpense-process}

Tu ajoutes le justificatif de la `::personnalExpense` dans le dossier _Mois_a_venir_ correspondant sur le [Nextcloud](https://nextcloud.savadenn.bzh/f/5737)


## Gestion des `::loadAccountReimbursements` {#loadAccountReimbursements-process}

::: note
Les étapes suivantes sont fait par le gérant pour le moment.
:::

Se connecter à la gestion bancaire et faire un virement par personne.

* Exemple de motif compte débité : PRET INITIATIVES GLD 202103
* Exemple de motif compte crédité : VIR SAVADENN INITIATIVES

Dans Le [portail MEG de fidouest](https://fidouest.mon-expert-en-gestion.fr/)

1. Aller dans le module **Achats**
1. Pour chaque personne, création d’un achat 
    * Type : Facture
    * Fournisseur : la personne 
    * Mode de réglement : **virement**
    * Date de la dépense : date du virement
    * Date d’échéance : date du virement
    * 1 ligne avec 
        * N° de compte : le compte de la personne 
        * Libéllé : le libéllé du virement compte débité
1. Cliquer sur `Sauvegarder` (**NE PAS** cliquer sur Enregistrer et Terminer)
1. Dans l’onglet **Réglements**
    * Mettre le n° de virement (qui apparait en développant l’item sous la consultation de compte bancaire)
    * Mettre la date du virement

::: warning
Attention cette dernière étape (Réglements) est à confirmer avec Fidouest car renseigner cette info passe automatiquement l’achat en réglé et non plus modifiable. Peut-être que Fidouest préfère faire cette étape
:::

## `::expenses` spécifiques

```include
processes/expenses/specific/mobile_carrier.md
```
