# Forfait mobile

Il est conseillé que tu aies un numéro de téléphone professionnel dédié.

1. Tu choisis un forfait mobile à ton nom
1. Tu payes le forfait personnellement
1. Chaque mois tu ajoutes le montant de tes communications à tes `::expenses` avec la `::personnalExpense`

::: tip
Un petit conseil, prend un opérateur mobile différent de ton fournisseur internet. Il te servira de back-up.
:::
