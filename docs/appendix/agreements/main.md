# Convention collective

Savadenn relève de la convention collective des [Bureaux d’Études Techniques, dite _Syntec_](https://www.syntec.fr/convention-collective/)

# Accords d’entreprise

Il n’y a pas d’accord d’entreprise en vigueur pour le moment.
