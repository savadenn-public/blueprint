# Risques professionnels

Ces documents sont gérés sur le [site de l’OIRA](https://oiraproject.eu/oira-tools/fr) avec le compte _oira@contact.savadenn.bzh_ (identifiants dans bitwarden).

- Ingénieur informatique :
  - [Document unique d’évaluation des risques professionnels](./docUniqueRisquePro_ingeieurInformatique.pdf)
