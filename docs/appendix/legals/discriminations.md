# Discriminations à l’embauche

_Code pénal Version en vigueur au 08 décembre 2020_

## Article 225-1

_Modifié par LOI n°2016-1547 du 18 novembre 2016 - art. 86_

Constitue une discrimination toute distinction opérée entre les personnes physiques sur le fondement de leur origine, de leur sexe, de leur situation de famille, de leur grossesse, de leur apparence physique, de la particulière vulnérabilité résultant de leur situation économique, apparente ou connue de son auteur, de leur patronyme, de leur lieu de résidence, de leur état de santé, de leur perte d’autonomie, de leur handicap, de leurs caractéristiques génétiques, de leurs mœurs, de leur orientation sexuelle, de leur identité de genre, de leur âge, de leurs opinions politiques, de leurs activités syndicales, de leur capacité à s'exprimer dans une langue autre que le français, de leur appartenance ou de leur non-appartenance, vraie ou supposée, à une ethnie, une Nation, une prétendue race ou une religion déterminée.

Constitue également une discrimination toute distinction opérée entre les personnes morales sur le fondement de l’origine, du sexe, de la situation de famille, de la grossesse, de l’apparence physique, de la particulière vulnérabilité résultant de la situation économique, apparente ou connue de son auteur, du patronyme, du lieu de résidence, de l’état de santé, de la perte d’autonomie, du handicap, des caractéristiques génétiques, des mœurs, de l’orientation sexuelle, de l’identité de genre, de l’âge, des opinions politiques, des activités syndicales, de la capacité à s'exprimer dans une langue autre que le français, de l’appartenance ou de la non-appartenance, vraie ou supposée, à une ethnie, une Nation, une prétendue race ou une religion déterminée des membres ou de certains membres de ces personnes morales.

## Article 225-1-1

_Création LOI n°2012-954 du 6 août 2012 - art. 3_

Constitue une discrimination toute distinction opérée entre les personnes parce qu’elles ont subi ou refusé de subir des faits de harcèlement sexuel tels que définis à l’article 222-33 ou témoigné de tels faits, y compris, dans le cas mentionné au I du même article, si les propos ou comportements n’ont pas été répétés.
Article 225-1-2

_Création LOI n°2017-86 du 27 janvier 2017 - art. 177_

Constitue une discrimination toute distinction opérée entre les personnes parce qu’elles ont subi ou refusé de subir des faits de bizutage définis à l’article 225-16-1 ou témoigné de tels faits.

## Article 225-2

_Modifié par LOI n°2017-86 du 27 janvier 2017 - art. 177_

La discrimination définie aux articles 225-1 à 225-1-2, commise à l’égard d’une personne physique ou morale, est punie de trois ans d’emprisonnement et de 45 000 euros d’amende lorsqu’elle consiste :

1° A refuser la fourniture d’un bien ou d’un service ;

2° A entraver l’exercice normal d’une activité économique quelconque ;

3° A refuser d’embaucher, à sanctionner ou à licencier une personne ;

4° A subordonner la fourniture d’un bien ou d’un service à une condition fondée sur l’un des éléments visés à l’article 225-1 ou prévue aux articles 225-1-1 ou 225-1-2 ;

5° A subordonner une offre d’emploi, une demande de stage ou une période de formation en entreprise à une condition fondée sur l’un des éléments visés à l’article 225-1 ou prévue aux articles 225-1-1 ou 225-1-2 ;

6° A refuser d’accepter une personne à l’un des stages visés par le 2° de l’article L. 412-8 du code de la sécurité sociale.

Lorsque le refus discriminatoire prévu au 1° est commis dans un lieu accueillant du public ou aux fins d’en interdire l’accès, les peines sont portées à cinq ans d’emprisonnement et à 75 000 euros d’amende.

## Article 225-3

_Modifié par LOI n°2016-1547 du 18 novembre 2016 - art. 86_

Les dispositions de l’article précédent ne sont pas applicables :

1° Aux discriminations fondées sur l’état de santé, lorsqu’elles consistent en des opérations ayant pour objet la prévention et la couverture du risque décès, des risques portant atteinte à l’intégrité physique de la personne ou des risques d’incapacité de travail ou d’invalidité. Toutefois, ces discriminations sont punies des peines prévues à l’article précédent lorsqu’elles se fondent sur la prise en compte de tests génétiques prédictifs ayant pour objet une maladie qui n’est pas encore déclarée ou une prédisposition génétique à une maladie ou qu’elles se fondent sur la prise en compte des conséquences sur l’état de santé d’un prélèvement d’organe tel que défini à l’article L. 1231-1 du code de la santé publique ;

2° Aux discriminations fondées sur l’état de santé ou le handicap, lorsqu’elles consistent en un refus d’embauche ou un licenciement fondé sur l’inaptitude médicalement constatée soit dans le cadre du titre IV du livre II du code du travail, soit dans le cadre des lois portant dispositions statutaires relatives à la fonction publique ;

3° Aux discriminations fondées, en matière d’embauche, sur un motif mentionné à l’article 225-1 du présent code, lorsqu’un tel motif constitue une exigence professionnelle essentielle et déterminante et pour autant que l’objectif soit légitime et l’exigence proportionnée ;

4° Aux discriminations fondées, en matière d’accès aux biens et services, sur le sexe lorsque cette discrimination est justifiée par la protection des victimes de violences à caractère sexuel, des considérations liées au respect de la vie privée et de la décence, la promotion de l’égalité des sexes ou des intérêts des hommes ou des femmes, la liberté d’association ou l’organisation d’activités sportives ;

5° Aux refus d’embauche fondés sur la nationalité lorsqu’ils résultent de l’application des dispositions statutaires relatives à la fonction publique ;

6° Aux discriminations liées au lieu de résidence lorsque la personne chargée de la fourniture d’un bien ou service se trouve en situation de danger manifeste.

Les mesures prises en faveur des personnes résidant dans certaines zones géographiques et visant à favoriser l’égalité de traitement ne constituent pas une discrimination.

## Article 225-3-1

_Création Loi n°2006-396 du 31 mars 2006 - art. 45 () JORF 2 avril 2006_

Les délits prévus par la présente section sont constitués même s'ils sont commis à l’encontre d’une ou plusieurs personnes ayant sollicité l’un des biens, actes, services ou contrats mentionnés à l’article 225-2 dans le but de démontrer l’existence du comportement discriminatoire, dès lors que la preuve de ce comportement est établie.
Article 225-4

_Modifié par LOI n°2009-526 du 12 mai 2009 - art. 124_

Les personnes morales déclarées responsables pénalement, dans les conditions prévues par l’article 121-2, des infractions définies à l’article 225-2 encourent, outre l’amende suivant les modalités prévues par l’article 131-38, les peines prévues par les 2° à 5°, 8° et 9° de l’article 131-39.

l’interdiction mentionnée au 2° de l’article 131-39 porte sur l’activité dans l’exercice ou à l’occasion de l’exercice de laquelle l’infraction a été commise.
