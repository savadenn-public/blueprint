# Affichage légal

## Inspection du travail {#work-inspector}

- Commune : Auray
- Département : 56
- Unité de Contrôle : 56U01
- Section_interpro: 56E3
- Tel: 02.97.26.26.95
- Mail: [bretag-ut56.uc1-3@direccte.gouv.fr](mailto:bretag-ut56.uc1-3@direccte.gouv.fr)

## Défenseur des droits {#rights-defender}

Téléphone : 09 69 39 00 00

::: Note
Demandes d’information et de conseil sur les discriminations et sur les conditions de saisine du Défenseur des droits
:::

## Médecine du travail {#work-medic}

// todo

## Informations syndicales

::: note
Affichage des informations syndicales selon conditions fixées par accord avec l’employeur
:::

## Horaires collectifs de travail

Les horaires collectifs de travail pour un salarié qui ne bénéficierai pas des horaires individualisés est 09:00 - 12:00 / 14:00 - 18:00.

```{.include shift-heading-level-by=1}
appendix/legals/discriminations.md
appendix/legals/sexual-harassment.md
appendix/legals/moral-harassment.md
appendix/legals/gender-equality.md
```
