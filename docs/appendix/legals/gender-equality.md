# Égalité professionnelle et salariale entre hommes et femmes

[Source Legifrance](https://www.legifrance.gouv.fr/codes/id/LEGIARTI000006902818/2008-05-01/)

_Code du travail Version en vigueur au 01 mai 2008_

## Article L3221-1

Les dispositions des articles L. 3221-2 à L. 3221-7 sont applicables, outre aux employeurs et salariés mentionnés à l’article L. 3211-1, à ceux non régis par le code du travail et, notamment, aux agents de droit public.

## Article L3221-2

Tout employeur assure, pour un même travail ou pour un travail de valeur égale, l’égalité de rémunération entre les femmes et les hommes.

## Article L3221-3

Constitue une rémunération au sens du présent chapitre, le salaire ou traitement ordinaire de base ou minimum et tous les autres avantages et accessoires payés, directement ou indirectement, en espèces ou en nature, par l’employeur au salarié en raison de l’emploi de ce dernier.

## Article L3221-4

Sont considérés comme ayant une valeur égale, les travaux qui exigent des salariés un ensemble comparable de connaissances professionnelles consacrées par un titre, un diplôme ou une pratique professionnelle, de capacités découlant de l’expérience acquise, de responsabilités et de charge physique ou nerveuse.

## Article L3221-5

Les disparités de rémunération entre les établissements d’une même entreprise ne peuvent pas, pour un même travail ou pour un travail de valeur égale, être fondées sur l’appartenance des salariés de ces établissements à l’un ou l’autre sexe.

## Article L3221-6

Les différents éléments composant la rémunération sont établis selon des normes identiques pour les femmes et pour les hommes.
Les catégories et les critères de classification et de promotion professionnelles ainsi que toutes les autres bases de calcul de la rémunération, notamment les modes d’évaluation des emplois, doivent être communs aux salariés des deux sexes.

## Article L3221-7

Est nulle de plein droit toute disposition figurant notamment dans un contrat de travail, une convention ou accord collectif de travail, un accord de salaires, un règlement ou barème de salaires résultant d’une décision d’un employeur ou d’un groupement d’employeurs et qui, contrairement aux articles L. 3221-2 à L. 3221-6, comporte, pour un ou des salariés de l’un des deux sexes, une rémunération inférieure à celle de salariés de l’autre sexe pour un même travail ou un travail de valeur égale.
La rémunération plus élevée dont bénéficient ces derniers salariés est substituée de plein droit à celle que comportait la disposition entachée de nullité.
