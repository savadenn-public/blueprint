# Harcèlement moral

_Code pénal Version en vigueur au 08 décembre 2020_

## Article 222-33-2

_Modifié par LOI n°2014-873 du 4 août 2014 - art. 40_

Le fait de harceler autrui par des propos ou comportements répétés ayant pour objet ou pour effet une dégradation des conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d’altérer sa santé physique ou mentale ou de compromettre son avenir professionnel, est puni de deux ans d’emprisonnement et de 30 000 € d’amende.

## Article 222-33-2-1

_Modifié par LOI n°2020-936 du 30 juillet 2020 - art. 9_

Le fait de harceler son conjoint, son partenaire lié par un pacte civil de solidarité ou son concubin par des propos ou comportements répétés ayant pour objet ou pour effet une dégradation de ses conditions de vie se traduisant par une altération de sa santé physique ou mentale est puni de trois ans d’emprisonnement et de 45 000 € d’amende lorsque ces faits ont causé une incapacité totale de travail inférieure ou égale à huit jours ou n’ont entraîné aucune incapacité de travail et de cinq ans d’emprisonnement et de 75 000 € d’amende lorsqu’ils ont causé une incapacité totale de travail supérieure à huit jours ou ont été commis alors qu’un mineur était présent et y a assisté.

Les mêmes peines sont encourues lorsque cette infraction est commise par un ancien conjoint ou un ancien concubin de la victime, ou un ancien partenaire lié à cette dernière par un pacte civil de solidarité.

Les peines sont portées à dix ans d’emprisonnement et à 150 000 € d’amende lorsque le harcèlement a conduit la victime à se suicider ou à tenter de se suicider.

## Article 222-33-2-2

_Modifié par LOI n°2018-703 du 3 août 2018 - art. 11_
_Modifié par LOI n°2018-703 du 3 août 2018 - art. 13_

Le fait de harceler une personne par des propos ou comportements répétés ayant pour objet ou pour effet une dégradation de ses conditions de vie se traduisant par une altération de sa santé physique ou mentale est puni d’un an d’emprisonnement et de 15 000 € d’amende lorsque ces faits ont causé une incapacité totale de travail inférieure ou égale à huit jours ou n’ont entraîné aucune incapacité de travail.

l’infraction est également constituée :

a) Lorsque ces propos ou comportements sont imposés à une même victime par plusieurs personnes, de manière concertée ou à l’instigation de l’une d’elles, alors même que chacune de ces personnes n’a pas agi de façon répétée ;

b) Lorsque ces propos ou comportements sont imposés à une même victime, successivement, par plusieurs personnes qui, même en l’absence de concertation, savent que ces propos ou comportements caractérisent une répétition.

Les faits mentionnés aux premier à quatrième alinéas sont punis de deux ans d’emprisonnement et de 30 000 € d’amende :

1° Lorsqu’ils ont causé une incapacité totale de travail supérieure à huit jours ;

2° Lorsqu’ils ont été commis sur un mineur de quinze ans ;

3° Lorsqu’ils ont été commis sur une personne dont la particulière vulnérabilité, due à son âge, à une maladie, à une infirmité, à une déficience physique ou psychique ou à un état de grossesse, est apparente ou connue de leur auteur ;

4° Lorsqu’ils ont été commis par l’utilisation d’un service de communication au public en ligne ou par le biais d’un support numérique ou électronique ;

5° Lorsqu’un mineur était présent et y a assisté.

Les faits mentionnés aux premier à quatrième alinéas sont punis de trois ans d’emprisonnement et de 45 000 € d’amende lorsqu’ils sont commis dans deux des circonstances mentionnées aux 1° à 5°.
