# Définitions

## Le métier : _Pourquoi_ {#business-title}

`métier`{#business alias-for=business-title}

::: {def-for=business-title}
C’est le **contexte** du `::project`. C’est-à-dire l’ensemble des informations relatifs à l’environnement dans lequel le projet se situe.
:::

Il est documenté dans le `::project`, en particulier :

- le **périmètre métier** : c’est la **frontière** entre l’intérieur et l’extérieur du `::project`
- la **localisation** du `::project` dans la **chaine de valeur** du `::client`
- les **interactions** du `::project` avec son extérieur

::: note
Le **périmètre métier** est clairement **défini** et évolue seulement avec l’accord de tout le monde.

Le plus souvent, un projet devient trop important et l’`::team` le découpe en plusieurs `::projects` : il y a donc une redéfinition de chaque périmètre.
:::

## L’équipe : _Par qui_ {#team-title}

`équipe`{#team alias-for=team-title}

::: {def-for=team-title}

Un `::project` est porté par une `::team` **dédiée**, composée de :

- plusieurs `::teammates`
- un `::project-lead`

:::

`équipier`{#teammate}
`équipiers`{#teammates alias-for=teammate}

::: {def-for=teammate}
Un `::actor` membre d’une `::team` avec un **temps défini et réservé** pour le `::project`.
:::

`porteur de projet`{#project-lead}
`porteurs de projet`{#project-leads alias-for=project-lead}

::: {def-for=project-lead}
Un `::actor` qui anime la `::team` et est responsable de la roadmap du `::project`.
:::

## Le client : _Pour qui_ {#client-title}

`client`{#client alias-for=client-title}
`clients`{#clients alias-for=client-title}

::: {def-for=client-title}
C’est la cible du `::project`. Le `::project` a un **impact** dans sa **chaine de valeur** et il **finance** le plus souvent le `::project`.
:::

Il a la **connaissance de son métier** qu’il transmet à l’`::team` au travers d’un `::business-owner`. Celui-ci peut-être intégré à l’`::team` s'il possède un temps dédié à ce `::project` et s'intègre dans notre mode d’organisation.

::: tip
Il est différent des `::users`. On parle ici d’organisation de `::project`, pas de persona.
:::

`référent métier`{#business-owner}
`référents métier`{#business-owners alias-for=business-owner}

::: {def-for=business-owner}
Porteur de la connaissance du métier de ce `::project`
:::

## La tâche : _Comment_ {#issue-title}

`tâche`{#issue alias-for=issue-title}
`tâche`{#task alias-for=issue-title}
`tâches`{#issues alias-for=issue-title}
`tâches`{#tasks alias-for=issue-title}

::: {def-for=issue-title}
Une tache, _appelée [Issue](https://docs.gitlab.com/ee/user/project/issues/) sur `::gitlab`_, est l’**unité atomique** d’un `::project`. Elle est traitée que comme un tout : **réalisé** ou **non réalisée**.
:::

Il n’y a pas de notion d’avancement de tâche. Elle contient un descriptif des actions à réaliser ainsi que toutes les discussions relatives à cette tâche.

## La Merge-Request : _Quoi_ {#mr-title}

`Merge-Request`{#mr alias-for=mr-title}
`Merge-Requests`{#mrs alias-for=mr-title}

::: {def-for=mr-title}
C’est un concept propre à `::gitlab` qui trace une demande de `::merge`. Elles peuvent être liées à une ou plusieurs `::issues`.
:::

::: note
[Documentation Gitlab](https://docs.gitlab.com/ee/user/project/merge_requests/) sur les `::mrs`
:::

::: tip
Équivalent à une _Pull-Request_ de _Github_
:::
