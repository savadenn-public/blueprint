# Le projet {#project-title}

`projet`{#project alias-for=project-title}
`projets`{#projects alias-for=project-title}

::: {def-for=project-title}
Le `::project` est l’ensemble composé d’une `::team` agissant sur un périmètre `::business` défini pour un `::client`. Les actions nécessaires sont découpées en `::issue` et planifiées en `::milestone`.
:::

::: note
Les `::projects` sont organisés autour des principes des méthodologies Agile. Par expérience, la méthodologie Scrum est trop rigide pour notre organisation. Nous privilégions une organisation inspirée de la méthodologie Kanban.
:::

```include
project/definitions.md
project/roles.md
project/flow.md
project/issue.md
project/milestone.md
project/tensions.md
project/documentation.md
```
