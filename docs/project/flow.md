# Go with the flow {#flow-title}

`flow`{#flow alias-for=flow-title}

::: {def-for=flow-title}
Le `::flow` est le cycle de vie des `::issues` et des `::mrs` d’un `::project`. Il est géré dans `::gitlab` par des labels et `::milestones`.
:::

1. `::backlog`
1. `flow::todo`{#todo}

::: {def-for=todo}
* Tu priorises une `::task` :
    * tu l’affectes à une `::milestone`
    * [![Tag flow::todo](./tags/➟_flow-todo.svg)](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=%E2%9E%9F+flow%3A%3Atodo)
:::
  
4. `flow::doing`{#doing}
   
::: {def-for=doing}
* Tu signales que tu travailles sur une `::task` :
    * tu t’ajoute en `::assigned`    
    * [![Tag flow::doing](./tags/➟_flow-doing.svg)](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=%E2%9E%9F+flow%3A%3Adoing)
:::

5. `flow::done`{#done}

::: {def-for=done}
* La `::task` est terminée mais tu veux la garder ouverte (Ex : au cas où un `::client` voudrait ajouter un commentaire)
    * [![Tag flow::done](./tags/➟_flow-done.svg)](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=%E2%9E%9F+flow%3A%3Adone)
:::

6. `closed`{#closed}

## Assignation {#assigned-title}

`assigné`{#assigned alias-for=assigned-title}

::: {def-for=assigned}
Les `::actors` d’une `::issue` sont identifiés par le champ _assigné à_. Ce champ indique :

- `::todo` : l’`::actor` pressenti
- `::doing` : les `::actors` intervenant

Aussi, il faut mettre à jour cette information en fonction de son travail effectif.

::: tip
Tu peux te déassigner d’une `::issue` dans le `::backlog` pour garder une liste de `::tasks` pertinentes. 
:::

:::

## La priorité d’action

La priorité nominale est :

1. `::doing` auxquelles je suis `::assigned`
1. `::doing` sans `::assigned`
1. `::todo` auxquelles je suis `::assigned`, dans l’ordre haut → bas
1. `::todo` sans `::assigned`
1. Le reste

::: tip
Le [Sprint board](https://gitlab.com/groups/savadenn/-/boards/2340148) est une vue synthétique des priorités.
:::

## Les anomalies de cycle

### Le blocage

Si un évènement indépendant de l’`::team` bloque une tâche, l’`::team` rajoute le label [![Tag blocked::](./tags/blocked-_.svg)](https://gitlab.com/groups/savadenn/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=blocked%3A%3A), ou un des sous-labels si besoin.
La tâche reste dans le `::flow` qu’elle _bloque_ effectivement. L’`::team` doit tout mettre en œuvre pour lever les blocages.

Dans ce `::flow` il n’y a pas d’étape d’attente (Attente de réponse, Validation requise, etc). Le but est de privilégier le mouvement et de terminer ce qui est commencé en priorité. Un blocage est un événement perturbant qui doit être résolu.

Les différents tags de blocage :

* [![Tag blocked::](./tags/blocked-_.svg)](https://gitlab.com/groups/savadenn/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=blocked%3A%3A) : un blocage non-spécifique
* [![Tag blocked::CI](./tags/blocked-CI.svg)](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=blocked%3A%3ACI) : un blocage dû au `::ci`
* [![Tag blocked::postponed](./tags/blocked-postponed.svg)](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=blocked%3A%3Apostponed) : la `::issue` est décalée par son porteur
* [![Tag blocked::reply needed](./tags/blocked-reply_needed.svg)](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=blocked%3A%3Areply+needed) : une attente de réponde d’un tiers

::: important
Il y a une [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/12285) en cours pour la recherche par wildcard. Ex : `blocked::*`
:::

### L’annulation

L’`::team` peut annuler une tâche : tâche en doublon, actions rendues inutiles, erreurs de conception, etc.

Dans ce cas, la tâche est close et on ajoute le label [![Tag flow::cancelled](./tags/➟_flow-cancelled_⛔.svg)](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=➟ flow%3A%3Acancelled ⛔).
