# Les `::tensions`

Nous cherchons à nous améliorer en continu. Quoi de mieux que l’expérimentation ! Pour ce faire, nous avons défini un concept qui peut s'appliquer à toutes les situations ou quelquechose peut être changé, modifié, réparé, amélioré...

:::::: important

**La `tension`{#tension}**
`tensions`{#tensions alias-for=tension}

::: {def-for=tension}
C’est un ressenti personnel d’une situation qui ne nous convient pas quelle qu’en soit la raison.
:::

::::::

::: note

On s'inspire de la méthode Holacracy

- [BD Holacracy](https://labdsurlholacracy.com/bande-dessinee-holacracy#page-74-75)
- [Article](https://blog.holacracy.org/holacracy-basics-understanding-tensions-98fc3c032acf) sur les tensions en Holacracy

:::

## Matérialiser une `::tension`

Au cours d’un `::project`, tu vas forcément ressentir des `::tensions` vu que tout peut-être amélioré. Le cas échéant, il faut la signaler à l’`::team` pour pouvoir la traiter.

> Comment ?

1. Tu crées une `::issue` sur `::gitlab` dans le projet le plus pertinent.

   ::: note
   Tu peux avoir une `::tension` équivalente à une autre passée. C’est normal.

   C’est ta `::tension`, utilise une nouvelle `::issue` pour ça et ajoute des liens vers les `::tensions` pertinentes.
   :::

2. Tu décris ta `::tension` comme tu la ressens

   ::: tip
   Si tu le veux tu peux proposer des actions
   :::

3. Tu l’affectes au minimum à ton `::project-lead`
4. Tu ajoutes le _[label tension](https://gitlab.com/groups/savadenn/-/issues?label_name%5B%5D=tension)_
   ```markdown
   /label ~tension
   ```
5. Tu la places dans le `::sprint`
6. Tu communiques à ton `::team` ta nouvelle `::issue`

## Participer à la résolution d’une `::tension`

Si un de tes `::teammate` lève une `::tension`. Tu es invité à la lire et à proposer des actions allant dans le sens de sa résolution.

Tu peux avoir l’impression que cette `::tension` a déjà été traitée. Rappelle-toi qu’une `::tension` est un **ressenti personnel**. Essaie de comprendre avec ton `::teammate` ce qui est nouveau ou différent et note-le dans la `::issue` pour aider ton `::team` à la lever.

## Levée des `::tensions` {#close-tensions}

La levée des `::tensions` fait partie des actions d’un `::sprint`, au plus tard pendant la `::tensions-review`.

1. l’`::team` prend connaissance de la `::tension` en lisant la `::issue`
2. Chacun peut proposer des actions allant dans le sens de sa résolution

   ::: tip
   Tu es encouragé à proposer des changements d’organisation. C’est le principe même de Savadenn.
   :::

3. Est-ce ces actions rentrent dans le périmètre du `::project` ?

   ::: warning
   Si non, la `::tension` doit être transmis au `::project` concerné (ou une sous-partie après découpe)
   :::

4. l’`::team` planifie les actions à mener
5. Le `::project-lead` te demande si les actions choisies lèvent ta tension

   ::: success
   Si oui, on clôt le ticket.
   :::

   ::: error
   Si non, on itère.
   :::
