# La documentation {#doc-title}

`documentation`{#doc alias-for=doc-title}

::: {def-for=doc-title}
La `::doc` de chaque `::project` est organisé de 2 façons :

- La description de l’état est versionnée dans le projet et généré via [Pandoc](https://gitlab.com/savadenn-public/pandoc)
- La description du pourquoi est dans les `::issues` et les `::mrs`.

:::

::: note
Pour un projet critique, il peut être intéressant de procéder à l’export des `::mrs`, `::issues` et des commentaires associés.
:::

## Mettre en place la stack de `::doc`

```treeview
<project-folder>
├── .gitlab-ci.yml
├── docker-compose.yml # See
├── docs/
│   ├── main.md
|   ├── pandoc.yml
├── Makefile
```

Le dossier `docs/`{.treeview} contient à minima

- un `pandoc.yml`{.treeview} qui donne la configuration - _[Voir stable](https://gitlab.com/savadenn/organisation/blueprint/-/blob/stable/docs/pandoc.yml)_
- un `main.md`{.treeview} qui est le fichier d’entrée (d’autres sont chargés grâce à la balise ` ```includes`{.markdown}) - _[Voir stable](https://gitlab.com/savadenn/organisation/blueprint/-/blob/stable/docs/main.md)_

À la racine, tu rajoutes :

- un `docker-compose.yml`{.treeview} pour faire tourner la génération via Docker - _[Voir stable](https://gitlab.com/savadenn/organisation/blueprint/-/blob/stable/docker-compose.yml)_
- un `Makefile`{.treeview} pour lancer le tout avec une simple commande - _[Voir stable](https://gitlab.com/savadenn/organisation/blueprint/-/blob/stable/Makefile)_

## Activer l’`::ci` pour la `::doc`

Tu ajoutes un fichier `.gitlab-ci.yml`{.treeview} à ton projet, ou tu vérifies qu’il contient

```yaml
include:
   - project: savadenn-public/templates
     file:
        # See https://gitlab.com/savadenn-public/templates/-/blob/master/gitlab-ci/Base.ci.yml
        - /gitlab-ci/Base.ci.yml
        # Only if you want gitlab pages to serve documentation
        - /gitlab-ci/docs/Pages.yml
```

## Écrire la `::doc`

Tu l’écris dans le dossier `docs` en Markdown. Pour être exact, c’est le Markdown Pandoc qui a beaucoup plus de `::features` que celui de Github.

Tu peux trouver de l’aide pour mettre en forme la `::doc` dans :

- [Le projet Pandoc de Savadenn](https://savadenn-public.gitlab.io/pandoc/#exemples)
- [La doc officiel de Pandoc](https://pandoc.org/MANUAL.html#pandocs-markdown)

## Ajoutes un badge

Si tu es `::maintainer` de ton projet, tu peux ajouter un badge pour permettre un accès rapide à la `::doc`.

1. Dans `::gitlab` > `<ton projet>` > _Settings_ > _Général_
2. Dans _Badges_
   1. Name : "Read documentation"
   1. Link : _Lien vers le Pages du projet_
   1. Badge image URL : `https://img.shields.io/badge/Read-%{default_branch}-brightgreen?style=flat`
3. Clic _Add badge_

![Exemple d’ajout d’un badge pour lire la documentation](project/add_badge.png)
