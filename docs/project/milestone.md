# Milestone pour l’organisation temporelle {#milestone-title}

`milestone`{#milestone alias-for=milestone-title}
`milestones`{#milestones alias-for=milestone-title}

::: {def-for=milestone-title}
Une `::milestone` est un moyen de suivre les `::issues` et les `::mrs` réalisées ou à réaliser dans une période de temps donnée.

[Liste des milestones de Savadenn](https://gitlab.com/groups/savadenn/-/milestones)
:::

### Les `::milestones` spécifique

#### sprint {#sprint}

`sprints`{#sprints alias-for=sprint}

::: {def-for=sprint}
Le `::sprint` est la seule `::milestone` **obligatoire** de chaque `::project`. Le **titre** du `::sprint` est la **date de fin de sprint** au format `YYYY-MM-DD`.

Par défaut, le `::sprint` à une durée d’une semaine. Cette durée doit être modulée en fonction de la charge d’un `::project` et de la volonté de revue d’un `::client`.
:::

#### forum {#forum}

`forums`{#forums alias-for=forum}

:::::: {def-for=forum}
Le `::forum` regroupe des `::issue` qui permettent de garder des informations et de discuter sur un sujet donné. Il n’y a pas de priorisation de ses `::issue` et chacun est libre de participer.

C’est une mine d’information, n’hésite pas à chercher dedans.

L’intégralité des `::forum` Savadenn, [c’est ici](https://gitlab.com/groups/savadenn/-/milestones/32).

:::

#### backlog {#backlog}

::: {def-for=backlog}

C’est la `::milestone` qui contient toutes les `::issues` non-priorisées.

Tout le `::backlog` :

- [Savadenn](https://gitlab.com/groups/savadenn/-/milestones/26).
- [Savadenn Foss](https://gitlab.com/groups/savadenn-public/-/milestones/1).

:::

## La `::planning` {#planning-title}

`planification`{#planning alias-for=planning-title}

Chaque `::milestone` commence par une réunion de `::planning`.

::: {def-for=planning-title}
C’est le moment privilégié pour prévoir ce qu’on veut faire dans la `::milestone`.
:::

l’`::team` positionne les `::issues` qu’elle souhaite résoudre en priorité lors de cette `::milestone`. Le leitmotiv est : _Voila ce que nous voulons au moins faire !_

::: note
Bien évidemment, tu es libre d’aller chercher d’autres `::issues` dans le `::backlog` pendant la `::milestone`.
:::

Il vaut mieux faire une `::planning` légère pour avoir une meilleure vision des priorités.

## Démonstration {#demo-title}

`démo`{#demo alias-for=demo-title}

Parce qu’on est fier de ce que nous faisons, il est important de prévoir du temps pour montrer au `::client` les `::features` que nous avons réalisées lors d’une `::demo`.

::: tip
Et cela peut être l’occasion d’améliorer notre image en invitant des extérieurs au `::project`.
:::

## Fin et feedback {#end-title}

Nous prônons l’incrémental itératif. Aussi il est capital de faire un retour sur nous-même pour pouvoir évoluer. La fin de chaque `::milestone` (`::sprint` compris) est un moment privilégié pour faire le point sur l’état du `::project`.

Cette réunion de clôture est animée par le `::project-lead` et se décompose en 2 phases :

1. `revue de tâches`{#issues-review}

   ::: {def-for=issues-review}
   On fait le point sur toutes les `::issues` du `::project`. On passe rapidement sur toutes les `::issues` fermées ce qui permet d’apprécier le travail accompli. Puis, on décrit l’état des `::issues` en cours. On choisit soit de les déprioriser (retour au `::backlog`) soit de les mettre dans une autre `::milestone` (sauf les `::tensions`).
   :::

2. `revue de tensions`{#tensions-review}

   ::: {def-for=tensions-review}
   C’est le moment de traiter les `::tensions` qui n’ont pas été levées (les seules `::issues` ouvertes dans la `::milestone`). On procède une par une à la `::close-tensions`.
   :::

   ::: note
   Si certaines `::tensions` ne sont pas abordées par manque de temps, elles seront prioritaires pour la prochaine `::tensions-review`.
   :::
