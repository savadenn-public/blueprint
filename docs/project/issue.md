# Découper une tâche

Pour faciliter le `::flow`, une tâche doit être bien découpée. Elle doit contenir les actions nécessaires à sa résolution est seulement celles-ci.

Quelques règles pour aider à découper :

> Plus d’une journée : faut découper !

Très souvent, si la charge dépasse une journée cela indique que les actions peuvent être regroupées en plusieurs tâche

> Attentes anticipées, pas de tâche bloquée

Cas typique : faire un document administratif, attendre la réponse de l’état, faire une action. Il vaut mieux faire une tâche pour le document qui se termine quand celui-ci est envoyé. Et une deuxième qui débutera lors de la réception de la réponse.
