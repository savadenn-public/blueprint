# Rôles

::: note
Le générique masculin est utilisé sans aucune discrimination et uniquement dans le but d’alléger le texte.
:::

: Récapitulatif des rôles d’un `::project`

| Rôle               | Appartenance           |
| :----------------- | :--------------------- |
| `::teammate`       | `::team`               |
| `::project-lead`   | `::team`               |
| `::business-owner` | `::client` ou `::team` |
| `::actor`          |                        |
| `::user`           |                        |
