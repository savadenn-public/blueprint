# Automatique

Une partie de l’installation du poste de dév est automatisé grâce à [Ansible](https://www.ansible.com/) dans le projet [Infrastructure](https://gitlab.com/savadenn/tools/infrastructure)

## Pré-requis

```bash
# Install git
sudo apt update && sudo apt install git
# Checkout project
git clone https://gitlab.com/savadenn-public/ansible-roles/local-ansible-run
cd local-ansible-run
```

## Utilisation

1. Lancer un check de la machine
   ```bash
   cd local-ansible-run
   make workstation_check
   ```
2. Pour appliquer les changements conseillés
   ```bash
   make workstation_apply
   ```
