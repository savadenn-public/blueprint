# Intégration `::pulse`

Source : [Savadenn_Foss Echo_Cancel](https://gitlab.com/savadenn-public/pulseaudio-echo-cancel)

Ce guide explique comment installer et configurer la suppression d'écho audio avec PulseAudio_Echo_Cancel sous l'application Mumble.

## Installation de `::pulse`

Ouvrir un terminal et saisir la ligne suivante

```bash
sudo curl -fsSL -o /usr/local/bin/pulseaudio_echo_cancel "https://gitlab.com/savadenn-public/pulseaudio-echo-cancel/-/raw/master/configure.sh" && sudo chmod +x /usr/local/bin/pulseaudio_echo_cancel

```

## Executer et configurer `::pulse`

Lancer la commande :

```bash
pulseaudio_echo_cancel
```
`::pulse` va proposer les choix suivants : 

```bash
⚑ Select the relevant sound input
1) alsa_input.usb-Generic_Blue_Microphones_2109BAB0NJK8-00.iec958-stereo
2) alsa_input.pci-0000_00_1f.3.analog-stereo
```
Sélectionner l'entrée qui correspond au micro USB (dans l'exemple 1).

```bash
⚑ Select the relevant sound input
1) alsa_input.usb-Generic_Blue_Microphones_2109BAB0NJK8-00.iec958-stereo
2) alsa_input.pci-0000_00_1f.3.analog-stereo
```
Sélectionner la sortie audio qui correspond à votre configuration. 


## Configurer la sortie de votre périphérique de son

Lancer les paramètres du périphérique de son de votre machine.  

![Configuration de la sortie](workstation/installation/Output.png)

- OutputEchoCancel doit être sélectionné.
- Faire un test audio en augmentant le volume à l'aide des raccourcis clavier par exemple.
- Si le son est audible la configuration est bonne.

Dans le cas contraire :

- Sélectionner un autre périphérique de sortie et réaliser le test audio sur ce nouveau périphérique.
- Revenir sur le périphérique OutputEchoCancel et refaire le test audio.


## Configurer l'entrée de votre périphérique de son

![Configuration de l'entrée](workstation/installation/Input.png)

- InputEchoCancel doit être sélectionné.
- Faire un test de prise de son en tapotant sur le micro par exemple.
- Si la barre de niveau d'entrée réagi, la configuration est bonne.

Dans le cas contraire :

- Sélectionner un autre périphérique et réaliser le test audio sur ce nouveau périphérique.
- Revenir sur le périphérique OutputEchoCancel et refaire le test audio.
- Si nécessaire essayer avec d'autres périphériques.