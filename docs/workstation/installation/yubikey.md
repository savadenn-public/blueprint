# Intégration `::yubikey`

Source : [https://support.yubico.com/support/solutions/articles/15000011356-ubuntu-linux-login-guide-u2f](https://support.yubico.com/support/solutions/articles/15000011356-ubuntu-linux-login-guide-u2f)

## Introduction

Ce guide explique comment sécuriser le login Linux et les commandes `sudo` avec la fonction U2F des `::yubikeys`.

## Installation des dépendances

```bash
sudo add-apt-repository ppa:yubico/stable && sudo apt-get update
sudo apt-get install libpam-u2f pamu2fcfg
```

## Associer la clé U2F avec son compte

1. Ouvrir un terminal
2. Brancher la `::yubikey`
3. Lancer la commande

```bash
mkdir -p ~/.config/Yubico
pamu2fcfg > ~/.config/Yubico/u2f_keys
```

5. Quand la `::yubikey` clignote toucher la partie métallique.

## Configurer le système pour utiliser les clés U2F

```bash
sudo nano /etc/pam.d/common-auth
```

**Avant** toute instruction `auth`, insérer la ligne suivante :

```
auth       sufficient   pam_u2f.so
```

Sauvegarder le fichier et retirer la `::yubikey`. Ouvrir un **nouveau** terminal et taper la commande suivante :

```bash
sudo echo test
```

Quand demandé, saisir le mot de passe et valider par Entrer. La commande doit normalement bien s'exécuter.

Brancher la `::yubikey` et ouvrir un **nouveau** terminal. Rejouer la commande de test ci-dessus.
La `::yubikey` doit se mettre à clignoter. Toucher la partir métallique et la commande doit s'exécuter avec succès.

## Verrouiller la session quand la `::yubikey` est débranchée

Créer le script de verrouillage : `sudo nano /usr/local/bin/lockscreen.sh` :

```bash
#!/bin/sh

# This script, when called, locks session if yubikey is absent

sleep 2

if ! ykman info >> /dev/null 2>&1
then
  loginctl lock-sessions
fi
```

```bash
sudo chmod +x /usr/local/bin/lockscreen.sh
```

`sudo nano /etc/udev/rules.d/20-yubikey.rules` :

```bash
ACTION=="remove", ENV{SUBSYSTEM}=="usb", ENV{PRODUCT}=="1050/407/526", RUN+="/usr/local/bin/lockscreen.sh"
```

La valeur du paramètre `ENV{PRODUCT}` peut-être déterminée de cette manière :

```bash
# Brancher la yubikey
# monitor les actions systèmes
udevadm monitor --environment --udev

# Débrancher la yubikey, puis CTRL + C pour stopper le monitoring
```

Dans la sortie, trouver un bloc de lignes contenant la ligne `ID_VENDOR=Yubico` et contenant les entrées `ID_VENDOR_ID`, `ID_MODEL_ID` et `ID_REVISION`. \
Concatener ces 3 dernières valeurs, sans les `0` de gauche, séparés par des `/`. \
Par exemple, avec les lignes suivantes vous obtiendrez `1050/407/526` :

```
ID_VENDOR=Yubico
ID_VENDOR_ID=1050
ID_MODEL_ID=0407
ID_REVISION=0526
```

Enfin, recharger la conf :

```bash
sudo udevadm control --reload-rules
```
