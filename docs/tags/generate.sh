#!/usr/bin/env sh

# Process tags.yaml to generate images

DIR=/data/docs/tags

for color in $(yq eval 'keys | .[]'  "${DIR}"/list.yml); do
  for tag in $(yq eval '.["'${color}'"] | .[]' "${DIR}"/list.yml); do
    wget https://img.shields.io/badge/"${tag}"-white?labelColor="${color}" -O "${DIR}/${tag}.svg"
  done
done
