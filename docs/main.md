Le _Qui fait quoi, comment et pourquoi_ de Savadenn. Ce sont les plans.

Ces plans peuvent et doivent changer.

- J'ai une [idée](https://gitlab.com/savadenn/blueprint/-/issues/new) à discuter
- Je veux faire une proposition : Merge-Request

```includes
project/main.md
development/main.md
home-office.md
workstation/main.md
processes/main.md
location/main.md
glossary.md
appendix/main.md
```
