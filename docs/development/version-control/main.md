# Gestion de versions

Cette partie détaille nos méthodes de gestion de versions de nos livrables et de leurs sources.

Le type de gestion des livrables influe sur l’organisation de la gestion des sources. On distingue ainsi 2 modes: `::continuous-delivery` et `::release-delivery`.

En cas de doute: `::choose-delivery`.

```includes
development/version-control/git.md
development/version-control/continuous.md
development/version-control/releases.md
development/version-control/choose.md
```
