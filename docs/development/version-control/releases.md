# Release Delivery {#release-delivery}

`::release-delivery` ou _livraison par version_ est une approche d’ingénierie logicielle dans laquelle les équipes produisent et maintiennent plusieurs `::releases` d’un produit. Plus orienté vers la stabilité, il permet à des tiers une liberté dans l’intégration du produit (choix de version). Ils ont une visibilité sur la stabilité des `::releases`, le niveau de sécurité, et le calendrier de support.
