# Choisir son mode de livraison {#choose-delivery}

: Aide au choix du mode de livraison, le ✔ indique une préférence

| Questions                                                       | `::continuous-delivery` | `::release-delivery` |
| :-------------------------------------------------------------- | :---------------------: | :------------------: |
| Est-ce un projet à installation unique ?                        |            ✔            |                      |
| Est-ce un projet à installation multiple, ou par des tiers ?    |                         |          ✔           |
| Est-ce un projet intermédiaire, une librairie ?                 |                         |          ✔           |
| Souhaites-tu minimiser le `::ttm` ?                             |            ✔            |                      |
| As-tu besoin de gérer plusieurs versions en parallèle ?         |                         |          ✔           |
| As-tu besoin de communiquer sur les nouvelles fonctionnalités ? |                         |          ✔           |
