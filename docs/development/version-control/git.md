# Gestion des sources : Git {#git}

[Git](https://git-scm.com/) est notre outil pour gérer les versions des sources de nos projets. Il y a de nombreuses façon d’utiliser cet outil.
Ce guide rassemble nos règles d’utilisation.

## Vocabulaire

`commit`{#commit}, `commits`{#commits}
: Modification atomique du code :

    - Il référence un ensemble de fichiers modifiés
    - Il posséde un **message**.
    - Il est identifié par son **hash**.

`branche`{#branch}, `branches`{#branches}
: C’est un ensemble séquentiel de `::commits` avec un **nom**.

`merge`{#merge}
: C’est une action de fusion d’une `::branch` _source_ vers une `::branch` _cible_.

## Règles d’utilisation de git

### Tu signes tes `::commits`

Sécurité ! Nous devons être sûrs de la provenance des modifications de code.

::: note
Va voir [la procédure](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/) si besoin.
:::

### Tu indiques ton but dans ton message de `::commit`

On veut savoir _pourquoi_ tu fais ce changement. C’est inutile de dire comment, c’est déjà dans le code.

Si tu veux relier ton `::commit` à des `::issues`, utilises

- `fix #223` : pour fermer la `::issue` #223
- `see #223` : pour indiquer que ton `::commit` à un rapport avec cette `::issue`

:::::: note

::: success
Efficace : `Center button on mobile device, fix #223`
:::

::: error
Inutile : `Add justify-content: middle to external div`
:::

::::::

### Tu utilises seulement des minuscules pour nommer une `::branch`

Les caractères autorisés sont :

- lettres minuscules
- chiffres
- `/` pour les préfixes
- `-` pour remplacer les espaces

::: warning
Les espaces sont **interdits**. Ils génèrent beaucoup de problèmes en ligne de commande.
:::

:::::: note

Ansi tu peux rapidement trouver une branche même si son nom t'a été donné à l’oral.

::: success
Efficace : `ma-branche-de-travail`
:::

::: error
Inutile : `m4-brAnchE-2 Travail`
:::

::::::

### Tu utilises des préfixes pour le nom des `::branches` et des `::mrs`

- `feature/` pour les `::features`

  Si une `::branch` (ou `::mr`) contient des nouvelles `::features` :

  - C’est indiqué clairement par `feature/` dans son nom en préfixe.
  - Le nom synthétise la `::feature` pour avoir une idée du contenu sans fouiller dans le code

  :::::: note
  ::: success
  Efficace : `feature/one-click-to-contact-us`
  :::

  ::: error
  Inutile : `add-button-on-main-page`
  :::

  ::::::

- `fix/` pour les `::fix`

  Si une `::branch` (ou `::mr`) contient des `::fix` :

  - C’est indiqué clairement par `fix/` dans son nom en préfixe.
  - Le nom synthétise le but du `::fix`.

  :::::: note
  ::: success
  Efficace : `fix/responsive-contact-button-on-mobile`
  :::

  ::: error
  Inutile : `Change align-content: middle on span`
  :::

  ::::::

- `release/` pour les `::releases`
- `env/` pour les environnements
  - Exceptions : `production`, `staging`

### Tu nommes la `::branch` principale d’un projet en fonction de son utilité

: Exemples de `::branch` principale avec leur utilité

| Nom          | Utilité                                                                                  |
| :----------- | :--------------------------------------------------------------------------------------- |
| `dev`        | La `::branch` contient les derniers `::commits` mais rien ne garantie son fonctionnement |
| `stable`     | La `::branch` contient les dernières `::features` qui fonctionnent unitairement          |
| `production` | Contient le code déployé en production - _Cas du `::continuous-delivery`_                |
| `next`       | Contient la prochaine `::release` ouverte - _Cas du `::release-delivery`_                |

::: tip
l’usage de `master` est déprécié. Il ne permet pas de savoir quel est l’usage de la branche en question.
:::

::: note
Pour aller plus loin :

- [Naming master branch](https://www.kapwing.com/blog/how-to-rename-your-master-branch-to-main-in-git/)

:::

## Règles de `::mr`

De base :

- 1 commit de `::merge` par `::mr`, sans _fast-forward_
- Pas de [squash](https://git-scm.com/docs/git-rebase#_interactive_mode)
- Suppression de la `::branch` source une fois la `::mr` terminée

Le but de cette opération est de garder les `::branches` propres. Les `::commits` intermédiaires sont accessibles mais ne sont pas dans la `::branch` cible car ce ne sont pas des états stables.

![`::merge` sans et avec rebase](https://nvie.com/img/merge-without-ff@2x.png)

::: note
Le développeur qui effectue la `::mr` peut s'il le souhaite :

- Faire un _fast-forward_ si la `::branch` cible n’est pas une branche stable
- Squash les commits en cas de `::mr` avec peu de contenu

:::

::: note
Pour aller plus loin

- [Git flow vs Gitlab flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
- [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/)

:::
