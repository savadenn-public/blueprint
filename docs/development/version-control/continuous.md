# Continuous Delivery {#continuous-delivery}

`::continuous-delivery` ou _livraison en continu_ est une approche d’ingénierie logicielle dans laquelle les équipes produisent des logiciels dans des **cycles courts**, ce qui permet de le mettre à disposition à **n’importe quel moment**. Le but est de construire, tester et diffuser un logiciel le plus rapidement possible.

::: note
Définition [Wikipedia](https://fr.wikipedia.org/wiki/Livraison_continue)
:::

::: warning
Très différent du `::cd` qui décrit la façon de gérer les montées de versions.
:::
