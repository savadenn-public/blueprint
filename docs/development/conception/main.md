# Principes fondamentaux de conception

Il est important de définir les principes qui seront pris en compte dès la conception.
Ils sont désignés par le terme anglais `by design`{#by-design} (Ex : [Privacy by design](https://fr.wikipedia.org/wiki/Protection_de_la_vie_priv%C3%A9e_d%C3%A8s_la_conception))

Liste des règles :

```Concepts
rule.*
```

```includes
development/conception/conception-privacy.md
development/conception/conception-security.md
development/conception/conception-ux.md
```
