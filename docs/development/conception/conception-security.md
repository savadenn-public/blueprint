# La `::security` {#security-title}

`securité`{#security alias-for=security-title}
`securité by design`{#security-by-design alias-for=security-title}

Prendre en compte la sécurité dès la conception du système est nécessaire pour assurer la `::privacy-by-design`. Si la confidentialité impose des règles, la `::security-by-design` garantie qu’elles ne seront pas contournables.

::: important
La partie informatique est la plus connue de la `::security`.

La sécurité prend aussi en compte : l’infrastructure, les personnes, les pratiques et les processus.
:::

## Provenance : Identification, Authentification et Signature

Il faut être sûr de l’origine d’une donnée (ou une action). Pour ce faire, nous utilisons trois actions :

1. l’`identité`{#identity} : permet de savoir qui est qui
2. l’`authenticité`{#authenticity} : la preuve de l’`::identity`
3. La `signature`{#signature} : le lien entre une donnée et l’`::identity` de son émetteur.

Pour la suite de ce document, nous considérons `authentique`{#authentic} une donnée qui répond à ces 3 règles.

::: box
**`règle d’authentification`{#rule-of-authenticity}**

Je suis sûr de la provenance de chaque donnée.
:::

## `::integrity | capital` du système et des données {#integrity-title}

`intégrité`{#integrity alias-for=integrity-title}

Il faut être sûr que les données soient bien dans l’état où elles doivent être. 
Pour pouvoir faire confiance à un système ou une donnée, il faut être sûr de l’absence de modification illégitime (erreur, corruption ou falsification).
Le mieux est de concevoir un système qui en apporte la preuve.

::: box
**`règle d’intégrité`{#rule-of-integrity}**

Je peux prouver qu’une donnée ou un système n’a pas été modifiée.
:::

## Secret

Il faut être sûr que les données ne sont lisibles que par leurs destinataires (vérification de la `::rule-of-least-privileges`). Il ne s’agit pas seulement de bloquer l’accès mais de rendre les données inutilisables. C’est là que le chiffrement entre en jeu.

::: box
**`règle du secret`{#rule-of-secret}**

Seul le destinataire d’un message peut le lire.
:::
