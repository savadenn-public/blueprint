# La `::privacy` comme base de confiance {#privacy-title}

`confidentialité`{#privacy alias-for=privacy-title}
`confidentialité by design`{#privacy-by-design alias-for=privacy-title}

Afin de respecter les `::users` et la loi (cf `::gdpr`), nous devons intégrer les concepts de confidentialité dès la conception. 

C’est la demande de la CNIL dans ses [bons reflexes RGPD](https://www.cnil.fr/fr/protection-des-donnees-les-bons-reflexes) :

- `::privacy-relevance`
- `::privacy-transparency`
- `::privacy-rights`
- `::privacy-control`
- `::privacy-risk`
- `::privacy-security`

Pour chacun, nous définissons des **règles** qui seront utilisées pour la conception du `::project`.

## Ne collectez que les données vraiment nécessaires {#privacy-relevance}

::: tip
[La CNIL](https://www.cnil.fr/fr/protection-des-donnees-les-bons-reflexes) : Posez-vous les bonnes questions : Quel est mon objectif ? Quelles données sont indispensables pour atteindre cet objectif ? Ai-je le droit de collecter ces données ? Est-ce pertinent ? Les personnes concernées sont-elles d’accord ?
:::

Il n’y a pas de données récupérées par défaut ou par habitude. Chaque donnée entrée dans le système a un but précis documenté. Par exemple : _Pour quelles raisons aurais-je besoin de demander le genre d’une personne pour un site de vente ?_

L’avantage :

- moins de données = moins de contraintes
- une très bonne maitrise de la chaîne de transformation des données
- une traçabilité native : on sait d’où vient quoi

::: box
**`règle de pertinence`{#rule-of-relevance}**

Je sais à quoi sert chaque donnée.
:::

## Soyez transparent {#privacy-transparency}

::: tip
[La CNIL](https://www.cnil.fr/fr/protection-des-donnees-les-bons-reflexes) : Une information claire et complète constitue le socle du contrat de confiance qui vous lie avec les personnes dont vous traitez les données.
:::

Cette démarche d’informations de l’`::user` se rapproche de l’idée `::privacy-by-using`. Ce n’est pas une simple information contractuelle, comme les conditions générales que personne ne lit. Mais une démarche réelle pour la compréhension.

::: note
[`Privacy by using`{#privacy-by-using}](https://fr.wikipedia.org/wiki/Protection_de_la_vie_priv%C3%A9e_d%C3%A8s_la_conception#Vers_le_privacy_by_using)

Celui-ci repose sur le développement d’instruments technologiques, juridiques et informationnels permettant de développer une capacité d’apprentissage chez l’individu.
Il lui serait alors moins demandé d’agir que d’apprendre pour construire un comportement éclairé sur la base d’une meilleure connaissance de son environnement informationnel et des conséquences de ses comportements de divulgation (on parle d’empowerment).
:::

Le **but** est de créer de la confiance dans l’application.
Cette confiance sera d’autant plus forte qu’elle repose sur la compréhension réelle des outils (à contrario d’une confiance donnée à l’aveugle, ou au renom de la société).
Cela est encore plus valable pour des partenaires commerciaux : pas besoin d’audit, l’information est déjà préparée.

::: box
**`règle de transparence`{#rule-of-transparency}**

Je donne une information complète au propriétaire des données dans le but qu’il comprenne.
:::

## Pensez aux droits des personnes {#privacy-rights}

::: tip
[La CNIL](https://www.cnil.fr/fr/protection-des-donnees-les-bons-reflexes) : Vous devez répondre dans les meilleurs délais, aux demandes de consultation, de rectification ou de suppression des données.
:::

Le plus simple est de donner directement accès à leurs données aux `::users` afin qu’ils puissent les modifier à loisir. La `::rule-of-transparency` implique qu’ils comprennent les impacts des modifications.

::: box
**`règle des droits`{#rule-of-rights}**

Les données personnelles sont la propriété exclusive des `::users`.
:::

## Gardez la maîtrise des données {#privacy-control}

::: tip
[La CNIL](https://www.cnil.fr/fr/protection-des-donnees-les-bons-reflexes) : Le partage et la circulation des données personnelles doivent être encadrées et contractualisées, afin de leur assurer une protection à tout moment.
:::

C’est le résultat de la `::rule-of-relevance`. Je sais à quoi servent les données. Je connais les rôles de mes `::actors`. Donc je sais quel est l’ensemble de données strictement nécessaire à chacun.

C’est une **démarche constructive** :

1. Je n’ai accès à rien
1. J’ai besoin légitimement d’une donnée
1. Je gagne l’accès à la donnée pour un temps défini

::: box
**`règle d’accès`{#rule-of-least-privileges}**

Je peux accéder seulement aux données qui me sont strictement nécessaires et seulement après accord explicite de leurs propriétaires.
:::

Elle s’oppose à la **démarche destructrice** :

1. J’ai accès à tout
1. On me bloque l’accès quand je n’en ai pas besoin.

::: tip
Ex : Si quelqu’un veut vous emprunter un livre, vous lui prêtez ce livre. Vous ne lui donnez pas les clés de votre maison. À charge à lui de prendre le livre en question _et rien d’autre_.
:::

## Identifiez les risques {#privacy-risk}

::: tip
[La CNIL](https://www.cnil.fr/fr/protection-des-donnees-les-bons-reflexes) : Vous traitez énormément de données, ou bien des données sensibles ou avez des activités ayant des conséquences particulières pour les personnes, des mesures spécifiques peuvent s’appliquer.
:::

::: box
**`règle des risques`{#rule-of-risks}**

Je connais les risques (causes, fréquences, conséquences) qui pèsent sur les données que je manipule.
:::

## Sécurisez vos données {#privacy-security}

::: tip
[La CNIL](https://www.cnil.fr/fr/protection-des-donnees-les-bons-reflexes) : Les mesures de sécurité, informatique mais aussi physique, doivent être adaptées en fonction de la sensibilité des données et des risques qui pèsent sur les personnes en cas d’incident.
:::

C’est ce qu’on nomme communément la `::security`.
