# L’`::ux` {#ux-title}

::: {def-for=ux}
L’`expérience utilisateur`{#ux}, _communément nommé UX pour _**U**ser e**X**perience_, est une démarche de création d’application comme un outil au service de son `::user`.
:::


Le but est de venir servir les buts de l’application en la rendant la plus rentable pour L’`::user`. C’est-à-dire que le coût d’utilisation (temps, concentration, mémoire) doit être inférieur aux gains (temps, qualité, valeur ajoutée).

## Intuitivité : distinction action & état

::: note
La « capacité d’un objet à suggérer sa propre utilisation », par exemple, sans qu’il ne soit nécessaire de lire un mode d’emploi. On parle aussi d’utilisation intuitive (ou du caractère intuitif) d’un objet.
_[Source Wikipedia](https://fr.wikipedia.org/wiki/Affordance)_
:::

L’`::user` reçoit des signaux physiologique au sein d’une application. Par fréquence d’apparition :

1. signaux optiques : couleur, forme (dont texte) et mouvement
2. signaux acoustiques : voix, musique, bruit
3. signaux haptiques : vibrations (plus rarement vibrations localisées)

L’`::user` trie ces signaux :

1. les __informations d’état__, décrivent l’état des choses (Ex : une date)
2. les __actions possibles__, indiquent les possibilités de changer l’état (Ex : un bouton)
3. le reste

Ce tri doit être le plus simple et le plus clair possible : Pas de confusion entre informations et actions.

De plus, il faut réduire le reste qui n’apporte rien et engendre un effort supplémentaire de tri d’information.

::: box
**`règle d’intuitivité`{#rule-of-affordance}**

l’`::user` identifie les informations d’état et les actions possibles sans erreur.
:::

::: tip
Le fait de limiter les actions de l’`::user` permet de simplifier son parcours en lui proposant un chemin à suivre et ainsi éviter toute surcharge d’informations.
:::

## Feedback : action - réaction

Lorsqu’on effectue une action, on s’attend à un changement dans notre environnement, principe d’action-réaction. De la même manière, l’application doit réagir après une action de l’`::user` car celui-ci s’attend de manière inconsciente à ce retour.

::: box
**`règle de feedback`{#rule-of-feedback}**

Après chaque action, l’`::user` a un retour.
:::

::: tip
Cette information peut prendre différentes formes (cf `::rule-of-affordance`) et peut convier des détails quant au succès de l’action.
:::

## La cohérence et les standards

Suivant la `::rule-of-affordance`, l’application doit mettre en œuvre une cohérence des signaux. Un même signal doit transmettre le même type d’information pour l’`::user`.

De la même manière, il est conseillé de prendre en compte les signaux connus de l’utilisateur et de les intégrer, encore plus si ces signaux sont codifiés au sein de standards ou de normes.

::: note
Ex: 

* Message en rouge pour une erreur
* en vert pour une réussite
* la croix pour fermer
:::

::: box
**`règle des standards`{#rule-of-standards}**

L’application utilise les codes et les standards familiers de l’utilisateur et son environnement.
:::

## Les échappatoires

l’`::user` peut se perdre dans une application. Il doit toujours disposer d’un moyen simple de revenir en territoire connu. Il peut ainsi explorer sans peur de se retrouver coincé ou perdu.
De la même manière, pouvoir mettre en pause des actions et les recommencer plus tard permet plus de souplesse et favorise l’adoption de l’application.

::: box
**`règle de sortie de secours`{#rule-of-exit}**

L’`::user` peut à sa guise interrompre toute action et y revenir au moment le plus opportun.
:::
