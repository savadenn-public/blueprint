# `::ci | capital` {#ci-title}

`intégration continue`{#ci alias-for=ci-title}

::: {def-for=ci}
_Nommée aussi CI pour Continuous Integration_

> L’intégration continue est un ensemble de pratiques utilisées en génie logiciel consistant à vérifier à chaque modification de code source que le résultat des modifications ne produit pas de régression dans l’application développée.
> [Wikipedia](https://fr.wikipedia.org/wiki/Int%C3%A9gration_continue)
:::

Notre `::ci` s’exécute sur `::gitlab`. Le [projet Templates](https://gitlab.com/savadenn-public/templates) est la référence. Les fichiers de ce projet sont la base de la configuration de la `::ci` des autres.
