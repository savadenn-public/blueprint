# Continuous Deployment {#cd}

> Le _déploiement continu_ ou _Continuous deployment (CD)_ en anglais, est une approche d’ingénierie logicielle dans laquelle les fonctionnalités logicielles sont livrées fréquemment par le biais de déploiements automatisés.

[Définition Wikipedia](https://fr.wikipedia.org/wiki/D%C3%A9ploiement_continu)

Notre `::cd` est réalisé par les runners `::gitlab` sur la base de [Auto-Deploy](https://docs.gitlab.com/ee/topics/autodevops/stages.html#auto-deploy):

- Job [review](https://gitlab.com/savadenn-public/templates/-/blob/master/gitlab-ci/Deploy.ci.yml) : déploie un environment dynamique pour faire la `::review` d’une `::mr`
- Job [production](https://gitlab.com/savadenn-public/templates/-/blob/master/gitlab-ci/Deploy.ci.yml) : déploie l’environnement de production

Une [image](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image) construite par `::gitlab` utilise [helm](https://helm.sh/) pour déployer sur le cluster [Kubernetes](https://kubernetes.io/) correspondant à l’environnement souhaité.

En cas de déploiement spécifique, le `::project` contient

- un dossier `.kube/chart` : contenant la charte de déploiement
- un fichier `.kube/chart/<environnment>/values.yml`: valeurs propres à l’environnement

Le `::ci` est configuré avec les variables :

```yaml
variables:
  AUTO_DEVOPS_CHART: .kube/chart
  HELM_UPGRADE_VALUES_FILE: .kube/<environnment>/values.yaml
```

::: box

Exemple extrait du [.gitlab-ci.yml du site web](https://gitlab.com/savadenn/www-savadenn-bzh/-/blob/production/.gitlab-ci.yml)

```yaml
production:
  needs: ["kube_build", "web_build"]
  environment:
    name: production
    url: https://www.savadenn.bzh
  variables:
    AUTO_DEVOPS_CHART: .kube/chart
    HELM_UPGRADE_VALUES_FILE: .kube/production/values.yaml
```

:::
