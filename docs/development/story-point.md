# Story point {#storypoint}
Un `::storypoint` représente la complexité d'une `::issue` et non une estimation de temps.
La finalité est de pouvoir calculer des métriques telles que :

* la capacité de production d'une équipe 
* le temps moyen de résolution en fonction de la complexité
* le time-to-market en fonction de la complexité
* ...

La valeur minimale est de 1 et signifie que la `::issue` est triviale. Par exemple : envoyer un mail.  
La valeur maximale est de 8 et signifie que la `::issue` est complexe.
Entre les deux, la complexité de la `::issue` suit la suite de Fibonacci :

* 1 : trivial
* 2
* 3
* 5
* 8 : complexe

L'évaluation de la complexité d'une `::issue` se fait en équipe.

::: note
Une complexité élevée est peut-être le signe qu'il faut découper la `::issue` en sous-`::issues`.
:::

Une fois que l'équipe s'est mise d'accord sur la complexité, c'est au travers du poids de la `::issue` dans `::gitlab` qu'on lui affecte sa complexité.

![Affectation du poids d'une `::issue`](development/issue_weight.jpg)

