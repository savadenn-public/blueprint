# Review {#review}

La `::review` est une action de relecture d’une `::mr` par un `::actor` différent du développeur ayant ouvert la `::mr`. C’est un de tes `::teammates` qui relit tes `::mrs` et tu peux relire les leurs.
