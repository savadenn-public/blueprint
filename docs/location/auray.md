# l’atelier des entreprises {#ade}

Pépinière et hôtel d’entreprise à Auray.

- Site web : http://www.atelier-des-entreprises.fr

## Adresse

Atelier des Entreprises \
Place de l’Europe \
Porte Océane \
56400 Auray \
France

[Y aller](https://www.google.com/maps/dir//Atelier+des+Entreprises,+Porte+Oc%C3%A9ane,+Place+de+l’Europe,+56400+Auray/@47.6702911,-3.0109909,17z)

## Réserver une salle de réunion

`::ade` d’Auray met à disposition une plateforme de réservation des salles de réunions :
http://reservations.atelier-des-entreprises.fr

Les identifiants pour s'y connecter sont transmis par l’ADE via la fiche nouvel arrivant.

## On mange où (Auray) ?

On liste ici les bonnes adresses où manger sur Auray.

| Nom             | Description                                           | Vente à emporter |
| --------------- | ----------------------------------------------------- | ---------------- |
| Kabuki          | les meilleurs sushis de l’ouest !                     | oui              |
| Alrégal         | self-service. Accueil chaleureux et plats consistants | oui              |
| La Boucherie    | Franchise                                             | ?                |
| Le Rajasthan    | Indien                                                | oui              |
| CDE & Beer Zone | Bar à bière, burgers                                  | ?                |

## Installer l’imprimante réseau
Depuis l’interface de gestion des imprimantes, cliquer sur le bouton "+ Ajouter".

Dans "Imprimante réseau > Rechercher une imprimante réseau", saisir l’ip `10.10.0.10` et cliquer sur "Rechercher".
Choisir celle avec le numéro de port `9100` et cliquer sur "Suivant".

Le gestionnaire va alors tenter de récupérer automatiquement les drivers de l’imprimante.
Choisir le fabriquant `Kyocera`, puis le modèle `TASKalfa 500i KPDL` et le pilote `Generic PCL 6/PCL XL Printer Foomatic/pxlcolor`.

Une fois l’imprimante ajoutée, clic-droit > Propriétés. Dans "Options de l’imprimante", choisir la qualité d’impression "Haute qualité" et le mode couleur "Couleur".

Sauvegarder et c’est fini !