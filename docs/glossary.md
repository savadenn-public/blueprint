# Glossaire

`GitLab`{#gitlab}

::: {def-for=gitlab}

Notre outil de gestion de code et de projet, hébergé sur les serveurs de Gitlab
:::

- [Nos projets privée](https://gitlab.com/savadenn)
- [Nos projets Open-source](https://gitlab.com/savadenn-public)

:::::: note
[Détail des permissions en fonction du rôle](https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions)

`Maintainer`{#maintainer}
`Maintainers`{#maintainers alias-for=maintainer}

::: {dev-for=maintainer}
Un `::teammate` qui a des droits de configuration avancée sur le `::project`
:::

`developpeur`{#dev}
`developpeurs`{#devs alias-for=dev}

::: {dev-for=dev}
Un `::teammate` qui peut modifier un `::project` via des `::branches`. C’est un `::actor` de confiance.
:::
::::::

`Time-To-Market`{#ttm}

::: {def-for=ttm}

Délai entre le début de la conception d’une `::feature` et son utilisation par les `::users`.

[Wikipedia](https://en.wikipedia.org/wiki/Time_to_market)

:::

`Yubikey`{#yubikey}
`Yubikeys`{#yubikeys alias-for=yubikey}

::: {def-for=yubikey}

Clef physique d’authentification et chiffrement. Vendu par [Yubico](https://www.yubico.com/?lang=fr)

:::

`Pulse Audio Echo Cancel`{#pulse}

::: {def-for=pulse}

Logiciel de suppression d'écho afin d'améliorer le confort du chat vocal.

:::

`correctif`{#fix}
`correctifs`{#fixes alias-for=fix}

::: {def-for=fix}

_Fix_ en anglais. Modification d’un système dans le but de corriger une `::feature` défectueuse.

:::

`fonctionnalité`{#feature}
`fonctionnalités`{#features alias-for=feature}

::: {def-for=feature}

_Feature_ en anglais. Fonction implantée dans un système informatique permettant à l’utilisateur d’effectuer un traitement.

:::

`release`{#release}
`releases`{#releases alias-for=release}

::: {def-for=release}

Une version **stable** spécifique d’un projet à destination d’un tiers.

:::

`acteur`{#actor}
`acteurs`{#actors alias-for=actor}

::: {def-for=actor}
Les personnes ou groupes de personnes qui interviennent dans un `::project` ou sur le système.
:::

`utilisateur`{#user}
`utilisateurs`{#users alias-for=user}

::: {def-for=user}
Un `::actor` qui utilise une application dans un but défini.
:::

`MEG`{#meg}

::: {def-for=meg}
Le portail de notre cabinet comptable pour gérer nos achats, ventes, note de frais, salaires, etc.

https://fidouest.mon-expert-en-gestion.fr
:::

`note de frais`{#personnalExpense}

::: {def-for=personnalExpense}
Le remboursement d’une `::expense` effectuée par un salarié pour le compte de Savadenn. 

_Géré dans `::meg`_
:::

`comptable`{#accountant}

::: {def-for=accountant}
Personne qui gère la comptabilité de notre entreprise.
:::

`remboursement de compte courant d’associé`{#loadAccountReimbursement}
`remboursements de compte courant d’associé`{#loadAccountReimbursements alias-for=loadAccountReimbursement}

::: {def-for=loadAccountReimbursement}
Un remboursement à titre personnel du compte courant d’un associé. Exemple pour le remboursement d’un prêt d’honneur ou d’un apport en compte courant.
:::


`RGPD`{#gdpr}

::: {def-for=gdpr}

Acronyme pour _Règlement Général sur la Protection des Données_. Ensemble de **règles européennes** sur le traitement de toutes données personnelles.

Source officielle : [La CNIL](https://www.cnil.fr/fr/rgpd-de-quoi-parle-t-on)
:::

> La notion de « données personnelles » est à comprendre de façon très large
> Une « donnée personnelle » est « toute information se rapportant à une personne physique identifiée ou identifiable ».
> ...
> Le RGPD s’applique à toute organisation, publique et privée, qui traite des données personnelles pour son compte ou non, dès lors\ :
>
> - qu’elle est établie sur le territoire de l’Union européenne,
> - ou que son activité cible directement des résidents européens.
