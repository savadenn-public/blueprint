#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
export PROJECT_PATH
PROJECT_NAME := $(shell basename ${PROJECT_PATH})
export PROJECT_NAME
PROJECT_URL=${PROJECT_NAME}.docker.localhost
export PROJECT_URL
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

# command name that are also directories
.PHONY: dev tags

#-----------------------------------------
# Allow passing arguments to make
#-----------------------------------------
SUPPORTED_COMMANDS := dev run
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))
ifneq "$(SUPPORTS_MAKE_ARGS)" ""
  COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(COMMAND_ARGS):;@:)
endif

#-----------------------------------------
# Help commands
#-----------------------------------------
.PHONY: help
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

#-----------------------------------------
# Commands
#-----------------------------------------

clean: ## Cleans up environnement
	@mkdir -p ./public
	@rm -rf ./public/*
	@docker-compose down --remove-orphans
	@docker-compose pull

logo:
	@docker-compose run --rm logo

TARGET_DIR=${COMMAND_ARGS}
export TARGET_DIR
dev: clean ## Starts dev stack
	@docker-compose build --pull
	@echo "\nConnect to https://${PROJECT_URL}\n"
	@docker-compose up

run: ## Generates docs in all format in ./public
	@docker-compose run --rm -e "SPI_WATCH=false" -e "SPI_FORMAT=all" pandoc

tags: ## Generate gitlab tags image
	@docker run --rm -v "$(shell pwd)":/data --entrypoint sh mikefarah/yq  /data/docs/tags/generate.sh